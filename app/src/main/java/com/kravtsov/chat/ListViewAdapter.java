package com.kravtsov.chat;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by User on 09.11.2015.
 */
public class ListViewAdapter extends BaseAdapter{

    private ArrayList<SparseArray> Array;
    private Context context;

    public ListViewAdapter(Context context, ArrayList<SparseArray> myArray){
        this.context = context;
        this.Array = myArray;
    }

    @Override
    public int getCount() {
        return Array.size();
    }

    @Override
    public Object getItem(int position) {
        return Array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View customView = convertView;
        if (convertView == null) {
            LayoutInflater li = LayoutInflater.from(context);
            customView = li.inflate(R.layout.adapter_layout, null);
        }
            TextView TV_container = (TextView) customView
                    .findViewById(R.id.TV_container);
            TextView TV_NicName = (TextView) customView
                    .findViewById(R.id.TV_NicName);
            TextView TV_Timel = (TextView) customView
                    .findViewById(R.id.TV_Time);

            try {
                TV_container.setText(Array.get(position).get(0).toString());
                TV_NicName.setText(Array.get(position).get(1).toString());
                TV_Timel.setText(Array.get(position).get(2).toString());

            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG", "CustomGridAdapter проблема вывода " + ex.toString());
            }

        return customView;
    }

    public void AddtoArra(SparseArray newelement)
    {
        Array.add(newelement);
    }

}
