package com.kravtsov.chat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

/**
 * Created by User on 09.11.2015.
 */
public class NickName extends AppCompatActivity {

    public static final String APP_PREFERENCES = "preferences";
    public SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nickname);
        sp = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        final EditText ET = (EditText)findViewById(R.id.activity_nickname_ET);
        final TextView TV = (TextView)findViewById(R.id.activity_nickname_TV);
        final Context context = this;

        if (sp.contains("NickName")) {
            ET.setText(sp.getString("NickName", ""));
            TV.setText(getResources().getString(R.string.who_are_you_again));
        }

        findViewById(R.id.activity_nickname_BTN).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(ET.getText().toString().trim().length()>0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("NickName", ET.getText().toString().trim());
                        editor.apply();
                        startActivity(new Intent(context, MainActivity.class).putExtra("nick", ET.getText().toString().trim()));
                    }
                    else {
                        Toast.makeText(context, getString(R.string.problem_with_nick), Toast.LENGTH_LONG).show();
                        Random r = new Random();
                        int rand = r.nextInt(100);
                        ET.setText( getString(R.string.vasia)+rand);
                    }
                }
                catch (Exception ex){

                }
            }
        });
    }
}
