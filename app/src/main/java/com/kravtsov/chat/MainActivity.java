package com.kravtsov.chat;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private EditText ET;
    private ListView LV;
    ListViewAdapter LVA;

    private MulticastSocket multisocket;
    private InetAddress iadress;
    private String multicastAddress = "224.0.0.251";
    private int port = 5500;
    private Context context;

    private Boolean stop = true;

    server myServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        String name="Я особенный";
        try {
            Intent intent = getIntent();
            name = intent.getStringExtra("nick");
        }
        catch (Exception ex)
        {

        }

        ET = (EditText)findViewById(R.id.editText);
        LV = (ListView)findViewById(R.id.LV);
        LVA = new ListViewAdapter(this,new ArrayList<SparseArray>());
        LV.setAdapter(LVA);
        myServer = new server(name);

        stop = true;
        startClient();

        findViewById(R.id.Button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startServer(ET.getText().toString());
                ET.setText("");
            }
        });
    }

    private void startServer(final String text){
        new Thread(new Runnable() {

            @Override
            public void run() {
                if(text.length()>0) {
                    myServer.ShowMessage(text);
                }
            }
        }).start();
    }

    private void startClient() {
        try {
            iadress = InetAddress.getByName(multicastAddress);
            multisocket = new MulticastSocket(port);
            multisocket.joinGroup(iadress);
            multisocket.setBroadcast(true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                WifiManager wm = (WifiManager) getSystemService(context.WIFI_SERVICE);
                WifiManager.MulticastLock multicastLock = wm
                        .createMulticastLock("mylock");

                multicastLock.acquire();

                byte[] buf = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buf, buf.length,
                        iadress, port);
                while (stop) {

                    try {
                        multisocket.receive(packet);
                        Calendar c = Calendar.getInstance();
                        int hour = c.get(Calendar.HOUR_OF_DAY);
                        int minute = c.get(Calendar.MINUTE);
                        String time = time_HM_Show(hour,minute,":");
                        String inputstr = new String(packet.getData());
                        String [] MsgNic = inputstr.split("//__//");
                        SparseArray Element = new SparseArray();
                        Element.put(0, MsgNic[0]);
                        Element.put(1, MsgNic[1]);
                        Element.put(2, time);
                        LVA.AddtoArra(Element);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                LVA.notifyDataSetChanged();
                            }
                        });

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).start();
    }

    public String time_HM_Show(int Hour,int Minute,String separate)
    {
        String result="";
        if(Hour<10)
            result="0"+Hour;
        else
            result=""+Hour;

        if(Minute<10)
            result=result+separate+"0"+Minute;
        else
            result=result+separate+Minute;
        return result;
    }

    @Override protected void onDestroy() {
        super.onDestroy();

        stop = false;
    }
}
