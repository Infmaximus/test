package com.kravtsov.chat;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by User on 08.11.2015.
 */
public class server {
    private static MulticastSocket multisocket;
    private InetAddress iadress;
    private String multicastAddress = "224.0.0.251";
    private int port = 5500;
    private String nickname = "";
    public server(String nickname)
    {

        this.nickname = nickname;
    }

    public void ShowMessage(String mymessg)
    {
        String longstring=mymessg+"//__//"+nickname+"//__//";
        try{

            iadress = InetAddress.getByName(multicastAddress);
            multisocket = new MulticastSocket(port);
            multisocket.joinGroup(iadress);

            byte[] mensaje;
            if(longstring.length()>1024)
            {
                longstring = longstring.substring(0,1016);
            }
            mensaje = longstring.getBytes();
            DatagramPacket dp = new DatagramPacket(mensaje, mensaje.length,iadress,5500);
            multisocket.send(dp);
        }
        catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
